set.seed(37)
N <- 80000
theta <- c(0.0, pi/2.0, pi, 3*pi/2.0) #2*pi*runif(3)
xv <- 0.5 + 0.5*cos(theta) #c(0.5, 0.15, 0.0, 0.15, 0.5, 0.85, 1.0, 0.85) #c(0,0,1,1)
yv <- 0.5 + 0.5*sin(theta) #c(0.0, 0.15, 0.5, 0.85, 1.0, 0.85, 0.5, 0.15) #c(0,1,1,0)
colz <- c("orange", "seagreen", "steelblue", "white")

f1 <- function(x, y, v, alpha = 0.5)
{
    #xo <- alpha*x + (1-alpha)*xv[v] #sqrt(alpha*x*x + (1-alpha)*xv[v]*xv[v])
    #yo <- alpha*y + (1-alpha)*yv[v] #sqrt(alpha*y*y + (1-alpha)*yv[v]*yv[v])
    xo <- alpha*x + (1-alpha)*xv[v]
    yo <- alpha*y + (1-alpha)*yv[v]
    if(0.4*(xo-0.5)*(xo-0.5) + (yo-0.5)*(yo-0.5) < 0.01)
    {
        xo <- x
        yo <- y
    }
    return(list(x=xo, y=yo))
}

di <- function(x,y,x0,y0)
{
    sqrt((x-x0)*(x-x0) + (y-y0)*(y-y0))
}

doit <- function(v, a=0.5)
{
    x <- runif(1)
    y <- runif(1)
    for(i in 1:N)
    {
        #a=c(0.49+(i%%2)*0.02, 0.49+(i%%3)*0.01, 0.47+(i%%4)*0.02, 0.5)    
        k <- which.min(c(di(x,y,xv[1],yv[1]),di(x,y,xv[2],yv[2]),di(x,y,xv[3],yv[3]), di(x,y,xv[4],yv[4])))
        points(x,y, pch = 16, cex = 0.1, col = colz[k])
        li <- f1(x,y,v[i], a)
        x <- li$x
        y <- li$y
    }
}

plot(c(0,1), c(0,1), type = "n", xlab = "x", ylab = "y")
doit(sample(1:4, N, replace = TRUE))

q()
#doit(sample(c(1:4,2,2,2,2), 10000, replace = TRUE))

oppo <- function(i)
{
    1 + ((i+1) %% 4)
}

v <- 1:4
for (i in 5:N)
{
    s <- sample(1:4, 1)
    if(v[1]==v[3]) s <- sample(c(oppo(v[2]), v[2]),1)
    v <- c(s,v)
}

#doit(v[1:N])

