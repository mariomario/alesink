sink <- read.table("sink", header = FALSE)[,2:8]

generate_training_data <- function(N, M)
{
    is <- sample(1:nrow(sink), N, replace = FALSE)
	features <- data.frame(t(as.vector(unlist(t(sink[is,])))))
	for(j in 2:M)
	{
	    is <- sample(1:nrow(sink), N, replace = FALSE)
	    feature <- data.frame(t(as.vector(unlist(t(sink[is,])))))
        features <- rbind(features, feature)
    }
    return(features)
}

train <- generate_training_data(500, 4000)

write.csv(train, "train_an_autoencoder_on_this.csv", row.names = FALSE)

mu <- prcomp(train)
summary(mu)
pdf("prcomps.pdf")
plot(mu$sdev)

#library("tsne")
#tsne(train)
